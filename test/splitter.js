var Splitter = artifacts.require("./Splitter.sol");

contract('Splitter', function(accounts) {
  
  var instance;

  var owner = accounts[0];
  var bob = accounts[1]; 
  var carol = accounts[2];


  beforeEach(function(){
    return Splitter.new({from: owner})
    .then(function(_instance) {
      instance = _instance;
    });
  });


  it("should be owned by owner", function() {
    return instance.owner({from: owner})
    .then(function(_owner){
      assert.strictEqual(_owner, owner, "Contract is not owned by owner");
    });
  });


  it("should create a group, split 2 and balance of sender should be 0", function() {
    var amountSent = 2;
    return instance.createGroup(bob, carol, {from: owner})
    .then(function(txn){
      return instance.isSplitterGroup(owner)
      .then(function(_isIndeed) {
        assert(_isIndeed, true, "Group has not been created");
      })
      .then(function() {
        return instance.split({from: owner, value: amountSent})
      })
      .then(function(){
        return instance.getBalance(owner, {from: owner})
      })
      .then(function(_balance){
        assert.equal(_balance.toString(10), 0, "Balance of sender is not empty");
      });
    });
  });

  it("should split 2", function() {
    var bobBalanceBefore = web3.eth.getBalance(bob);
    var carolBalanceBefore = web3.eth.getBalance(carol);
    var amountSent = 2;

    return instance.createGroup(bob, carol, {from: owner})
    .then(function(txn){
       return instance.split({from: owner, value: amountSent})
      .then(function(txn2) {

        var bobCurrentBalance = web3.eth.getBalance(bob);
        assert.equal(bobBalanceBefore.plus(amountSent/2).toString(10) , bobCurrentBalance.toString(10), "Bob has a wrong balance ");

        var carolCurrentBalance = web3.eth.getBalance(carol);
        assert.equal(carolBalanceBefore.plus(amountSent/2).toString(10) , carolCurrentBalance.toString(10), "Carol has a wrong balance ");
      })
    });
  });

    it("should split 9 and send remaining 1 to sender", function() {
    var bobBalanceBefore = web3.eth.getBalance(bob);
    var carolBalanceBefore = web3.eth.getBalance(carol);
    var amountSent = 9;

    return instance.createGroup(bob, carol, {from: owner})
    .then(function(txn){
       return instance.split({from: owner, value: amountSent})
      .then(function(txn2) {

        var bobCurrentBalance = web3.eth.getBalance(bob);
        amountSent -= 1;
        console.log(amountSent);
        assert.equal(bobBalanceBefore.plus(amountSent/2).toString(10) , bobCurrentBalance.toString(10), "Bob has a wrong balance ");

        var carolCurrentBalance = web3.eth.getBalance(carol);
        assert.equal(carolBalanceBefore.plus(amountSent/2).toString(10) , carolCurrentBalance.toString(10), "Carol has a wrong balance ");
        
        return instance.remaining({from: owner})
        .then(function(_remaining) {
            assert.equal(_remaining, 0, " Remaining is not 0");
        });
      });

    });
  });




});
