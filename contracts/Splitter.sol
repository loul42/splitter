pragma solidity ^0.4.6;

contract Splitter {

    address owner;
    mapping (address => uint) public balances;
    
    event LogSplit(address sender, address address1, address address2);
    event LogWithdraw(address to, uint amount);  
    
   function Splitter()
        public
    {
        owner = msg.sender;
    }

    function split(address splitter1, address splitter2)
        public
        payable
        returns (bool success)
    {
        require(splitter1 != address(0));
        require(splitter2 != address(0));
        require(msg.value > 0);
        
        uint half = msg.value / 2;

        balances[splitter1] += half;
        balances[splitter2] += half;

        if (msg.value % 2 == 1 ){
            balances[msg.sender] += 1;
        }
        
        LogSplit(msg.sender, splitter1, splitter2);

        return true;
    }

    function withdraw()
        public
        returns(bool success) {

        uint amt = balances[msg.sender];

        require(amt > 0);

        balances[msg.sender] = 0;
        msg.sender.transfer(amt);
        LogWithdraw(msg.sender, amt);

        return true;
    }

    function killSwitch()
        public
        returns(bool success)
    {
        require(msg.sender == owner);
        selfdestruct(owner);
        return true;
    }

}